'use strict'

console.log("test.js");
const obj = require("./build/Release/facematcher");
const jimp = require("jimp");

console.log(obj.getVersion());
obj.init();
console.log("facematch init done");

var template0;
var template1;
var template2;
var template3;

jimp.read("who.jpg", function(err, jimpimg) {
    if (err) throw err;
    
    var buffer = new Buffer(jimpimg.bitmap.data);
    //template0 = Buffer.alloc(136);
    template0 = new Buffer(136);
    var sortfactor = obj.getTemplate(buffer, jimpimg.bitmap.width, jimpimg.bitmap.height, template0);
    console.log("who sortfactor " + sortfactor);
});

jimp.read("dude.jpg", function(err, jimpimg) {
    if (err) throw err;
    
    var buffer = new Buffer(jimpimg.bitmap.data);
    //template1 = Buffer.alloc(136);
    template1 = new Buffer(136);
    var sortfactor = obj.getTemplate(buffer, jimpimg.bitmap.width, jimpimg.bitmap.height, template1);
    console.log("dude sortfactor " + sortfactor);
    
});

jimp.read("guyok.jpg", function(err, jimpimg) {
    if (err) throw err;
    
    var buffer = new Buffer(jimpimg.bitmap.data);
    //template2 = Buffer.alloc(136);
    template2 = new Buffer(136);
    var sortfactor = obj.getTemplate(buffer, jimpimg.bitmap.width, jimpimg.bitmap.height, template2);
    console.log("guyok sortfactor " + sortfactor);
});

// this image is huge and will finish last
jimp.read("guy.jpg", function(err, jimpimg) {
    if (err) throw err;
    
    var buffer = new Buffer(jimpimg.bitmap.data);
    //template3 = Buffer.alloc(136);
    template3 = new Buffer(136);
    var sortfactor = obj.getTemplate(buffer, jimpimg.bitmap.width, jimpimg.bitmap.height, template3);
    console.log("guy sortfactor " + sortfactor);
    
    
    // all templates will be completed, find match for template3 (guy), should match template2
    var faces = [template0, template2, template1];
    var facematch = obj.findMatch(template3, faces);
    if (facematch == -1) console.log("no match found");
    else console.log("input template matches index " + facematch + " in template array [0-2]");
});

