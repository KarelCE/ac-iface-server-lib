{
    "targets": [
        {
            "target_name": "facematcher",
            "sources": ["facematcher.cpp"],
            "include_dirs": ["../iface/include", "<!(node -e \"require('nan')\")"],
            "libraries": ["-L../iface/linux64", "-L/usr/local/lib", "-liface", "-Wl,-R,."]
        }
    ]
}
