
#include "iface/include/iface.h"
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstring>

#include <nan.h>
using namespace v8;

bool _initted;
bool _ok;
void* _faceHandler;

void checkError(int errorCode, bool exitApp = false) {
    // If the returned error code is zero (defined as IFACE_OK) than
    // the calling of IFace API function was without errors. Otherwise error occurred and the error value must be evaluated.
    if (errorCode == IFACE_OK) {
        return;
    }

    // First special cases related to IFace SDK license errors are handled. If license error occurs then info
    // message with hardware ID is printed and application is terminated with error code.
    // Hardware ID can be retrieved by calling function IFACE_GetHardwareId.
    if (errorCode == IFACE_ERR_LICENSE_INTEGRATION_GENERIC) {
        int hwIdLen = 1024;
        char hwId[1024];
        IFACE_GetHardwareId(hwId, &hwIdLen);

        fprintf(stdout, "Your license is invalid or not present, please contact support for license with this HwId %s\n", hwId);

        if (exitApp) {
            std::exit(errorCode);
        }
    }

    // For other error code types, related error messages can be retrieved using function IFACE_GetErrorMessage
    // into prepared char buffer. Application is terminated with error code if input parameter exitApp is set to true.
    const int errorBufferLength = 1024;
    char errorBuffer[errorBufferLength];

    int getErrorMsgErrorCode = IFACE_GetErrorMessage( errorCode, errorBufferLength, errorBuffer );
    if( getErrorMsgErrorCode == IFACE_OK) {
        fprintf(stdout, "Error occurs! %s (code: %d)\n", errorBuffer, errorCode);

        if (exitApp) {
            std::exit(errorCode);
        }
    } else {
        fprintf(stdout, "Error occurs! Error occurred during error code to message translation (code: %d)\n", getErrorMsgErrorCode);
    }
}

NAN_METHOD(stop) {
    if (_ok) {
        checkError(IFACE_ReleaseEntity(_faceHandler), true);
        checkError(IFACE_Terminate(), true);
    }
    _ok = false;
    _initted = false;
}

NAN_METHOD(init) {
    _initted = false;
    _ok = true;

    //int arraySize = 512;
    //unsigned char* license = new unsigned char[arraySize];
    //int length = 0;

    //std::ifstream fin("iengine.lic");
    //if (fin.is_open()) {
    //    while (!fin.eof() && length < arraySize) {
    //        fin >> license[length];
    //        length++;
    //    }
    //}

    //if (IFACE_InitWithLicence((const unsigned char*)license, length) != IFACE_OK) {
    //    checkError(IFACE_InitWithLicence((const unsigned char*)license, length));
    if (IFACE_Init() != IFACE_OK) {
        checkError(IFACE_Init());
        _ok = false;
        fprintf(stdout, "Failed loading iFace library\n");
        return;
    }

    if (IFACE_CreateFaceHandler(&_faceHandler) != IFACE_OK) {
        checkError(IFACE_CreateFaceHandler(&_faceHandler));
        _ok = false;
        fprintf(stdout, "Failed creating iFace instance context\n");
        return;
    }

    if (IFACE_SetParam(_faceHandler, IFACE_PARAMETER_FACEDET_SPEED_ACCURACY_MODE, IFACE_FACEDET_SPEED_ACCURACY_MODE_ACCURATE) != IFACE_OK) {
        checkError(IFACE_SetParam(_faceHandler, IFACE_PARAMETER_FACEDET_SPEED_ACCURACY_MODE, IFACE_FACEDET_SPEED_ACCURACY_MODE_ACCURATE));
        _ok = false;
        fprintf(stdout, "Failed setting iFace parameter\n");
        return;
    }

    //delete[] license;
    _initted = true;
    fprintf(stdout, "FaceMatcher, iFace init OK\n");
}

//extern void getTemplate(unsigned char* data, int width, int height, char* templateData) {
NAN_METHOD(getTemplate) {
    if (!_initted) {
        fprintf(stdout, "Cannot process image when iFace library was not initialized\n");
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }
    if (!_ok) {
        fprintf(stdout, "Cannot process image when iFace library failed to load\n");
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }
    if (!info[0]->IsObject()) {
        fprintf(stdout, "1st argument of getTemplate() is not an object\n");
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }
    if (!info[1]->IsNumber()) {
        fprintf(stdout, "2nd argument of getTemplate() is not a number\n");
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }
    if (!info[2]->IsNumber()) {
        fprintf(stdout, "3rd argument of getTemplate() is not a number\n");
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }
    if (!info[3]->IsObject()) {
        fprintf(stdout, "4th argument of getTemplate() is not an object\n");
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }
    /*if (!info[4]->IsNumber()) {
        fprintf(stdout, "5th argument of getTemplate() is not a number\n");
        return;
    }*/
    
    int width = info[1]->NumberValue();
    int height = info[2]->NumberValue();
    
    Local<Object> bufferObj = info[0]->ToObject();
    unsigned char* data = (unsigned char*)node::Buffer::Data(bufferObj);
    fprintf(stdout, "buffer size %d, rgb32 size %d, rgb24 size %d\n", node::Buffer::Length(bufferObj), width * height * 4, width * height * 3);
    
    if (node::Buffer::Length(bufferObj) == width * height * 4) {
        fprintf(stdout, "Hold on to your butts, converting RGBA32 to RGB24\n");
        unsigned char* old = data;
        data = new unsigned char[width * height * 3];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                data[x*3 + y*width*3] = old[x*4 + y*width*4];
                data[x*3+1 + y*width*3] = old[x*4+1 + y*width*4];
                data[x*3+2 + y*width*3] = old[x*4+2 + y*width*4];
            }
        }
    }
    
    Local<Object> bufferObj2 = info[3]->ToObject();
    char* templateData = node::Buffer::Data(bufferObj2);

    int detectedFaces = 1; // iface reads initial value as maximum
    void** faces = new void*[detectedFaces];
    checkError(IFACE_CreateFace(&(faces[0])));
    void* face = faces[0];

    checkError(IFACE_DetectFaces(data, width, height, width*0.1f, width*0.9f, _faceHandler, &detectedFaces, faces));
    if (detectedFaces < 1) {
        fprintf(stdout, "No faces detected\n");
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }

    int size;
    checkError(IFACE_CreateTemplate(face, _faceHandler, 0, &size, NULL));
    if (size != 136) {
        fprintf(stdout, "face template size %d is not 136!\n", size);
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }
    //char* templateData = new char[size];
    checkError(IFACE_CreateTemplate(face, _faceHandler, 0, &size, templateData));
    
    fprintf(stdout, "generated template: <");
    for (int i = 0; i < 136; i++) {
        fprintf(stdout, "%02x ", templateData[i] & 0xff);
    }
    fprintf(stdout, ">\n");


    IFACE_FaceFeatureId* feats = new IFACE_FaceFeatureId[19];
    feats[0] = IFACE_FACE_FEATURE_ID_FACE_CHIN_TIP;
    feats[1] = IFACE_FACE_FEATURE_ID_FACE_LEFT_EDGE;
    feats[2] = IFACE_FACE_FEATURE_ID_FACE_RIGHT_EDGE;
    feats[3] = IFACE_FACE_FEATURE_ID_LEFT_EYEBROW_INNER_END;
    feats[4] = IFACE_FACE_FEATURE_ID_LEFT_EYEBROW_OUTER_END;
    feats[5] = IFACE_FACE_FEATURE_ID_LEFT_EYE_CENTRE;
    feats[6] = IFACE_FACE_FEATURE_ID_LEFT_EYE_INNER_CORNER;
    feats[7] = IFACE_FACE_FEATURE_ID_LEFT_EYE_OUTER_CORNER;
    feats[8] = IFACE_FACE_FEATURE_ID_RIGHT_EYEBROW_INNER_END;
    feats[9] = IFACE_FACE_FEATURE_ID_RIGHT_EYEBROW_OUTER_END;
    feats[10] = IFACE_FACE_FEATURE_ID_RIGHT_EYE_CENTRE;
    feats[11] = IFACE_FACE_FEATURE_ID_RIGHT_EYE_INNER_CORNER;
    feats[12] = IFACE_FACE_FEATURE_ID_RIGHT_EYE_OUTER_CORNER;
    feats[13] = IFACE_FACE_FEATURE_ID_MOUTH_CENTER;
    feats[14] = IFACE_FACE_FEATURE_ID_MOUTH_LEFT_CORNER;
    feats[15] = IFACE_FACE_FEATURE_ID_MOUTH_LOWER_EDGE;
    feats[16] = IFACE_FACE_FEATURE_ID_MOUTH_RIGHT_CORNER;
    feats[17] = IFACE_FACE_FEATURE_ID_MOUTH_UPPER_EDGE;
    feats[18] = IFACE_FACE_FEATURE_ID_NOSE_TIP;
    float* fX = new float[19];
    float* fY = new float[19];
    float* fscores = new float[19];
    checkError(IFACE_GetFaceFeatures(face, _faceHandler, feats, 19, fX, fY, fscores));
//    float x = fX[8] - fX[3];
//    float y = fY[8] - fY[3];
//    float browDistSqr = x*x + y*y;
    float x = fX[10] - fX[5];
    float y = fY[10] - fY[5];
    float eyeDistSqr = x*x + y*y;
//    x = fX[16] - fX[14];
//    y = fY[16] - fY[14];
//    float mouthWidthSqr = x*x + y*y;
    x = fX[2] - fX[1];
    y = fY[2] - fY[1];
    float faceWidthSqr = x*x + y*y;
    //float sortScore = (eyeDistSqr / browDistSqr) * (mouthWidthSqr / browDistSqr) * 10;
    float sortScore = faceWidthSqr / eyeDistSqr * 50;
    delete[] feats;
    delete[] fX;
    delete[] fY;
    delete[] fscores;
    
    if (node::Buffer::Length(bufferObj) == width * height * 4) {
        delete[] data;
    }

    delete[] faces;
    
    //return reinterpret_cast<void*>(templateData);
    //info.GetReturnValue().Set(Nan::NewBuffer(templateData, size).ToLocalChecked());
    info.GetReturnValue().Set(Nan::New<v8::Number>((int)sortScore));
}

bool isMatch(char* face1, char* face2) {
    float score;
    checkError(IFACE_MatchTemplate(_faceHandler, face1, face2, &score));

    if (score > 50) {
        return true;
    } else {
        return false;
    }
}

//extern int findMatch(char* faceInput, char* faceArray) {
NAN_METHOD(findMatch) {
    if (!_initted) {
        fprintf(stdout, "Cannot process image when iFace library was not initialized\n");
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }
    if (!_ok) {
        fprintf(stdout, "Cannot process image when iFace library failed to load\n");
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }
    if (!info[0]->IsObject()) {
        fprintf(stdout, "First argument of findMatch() is not an object\n");
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }
    if (!info[1]->IsArray()) {
        info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
        return;
    }
    
    Local<Object> bufferObj = info[0]->ToObject();
    char* faceInput = node::Buffer::Data(bufferObj);
    
    Local<Array> bufferArray = Local<Array>::Cast(info[1]);
    
    int faceCount = bufferArray->Length();
    
    fprintf(stdout, "input template: <");
    for (int i = 0; i < 136; i++) {
        fprintf(stdout, "%02x ", faceInput[i] & 0xff);
    }
    fprintf(stdout, ">\n");
    
    for (int i = 0; i < faceCount; i++) {
        char* faceCurrent = node::Buffer::Data( bufferArray->Get(i)->ToObject() );
        
        fprintf(stdout, "array template %d: <", i);
        for (int j = 0; j < 136; j++) {
            fprintf(stdout, "%02x ", reinterpret_cast<char*>(faceCurrent)[j] & 0xff);
        }
        fprintf(stdout, ">\n");
        
        if ( isMatch( reinterpret_cast<char*>(faceInput), reinterpret_cast<char*>(faceCurrent) ) ) {
            info.GetReturnValue().Set(Nan::New<v8::Number>(i));
            return;
        }
    }
    
    fprintf(stdout, "Searched %d faces, \n");
    info.GetReturnValue().Set(Nan::New<v8::Number>(-1));
    return;
}

NAN_METHOD(getVersion) {
    int major;
    int minor;
    int revision;

    int code = IFACE_GetVersionInfo(&major, &minor, &revision);

    std::stringstream ss;
    ss << "FaceMatcher v0.0.1, ";
    if (code == IFACE_OK) {
        ss << "iFace v" << major << "." << minor << "." << revision;
    } else {
        ss << "failed getting iFace version";
    }
    
    Local<String> v8string = String::NewFromUtf8(Isolate::GetCurrent(), ss.str().c_str());
    info.GetReturnValue().Set(v8string);
}

NAN_MODULE_INIT(init_node) {
    NAN_EXPORT(target, getVersion);
    NAN_EXPORT(target, init);
    NAN_EXPORT(target, getTemplate);
    NAN_EXPORT(target, findMatch);
}

NODE_MODULE(addon, init_node)
