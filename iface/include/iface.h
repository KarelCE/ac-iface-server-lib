/* This header file contains IFace SDK API functions
   declarations.                                     */
#ifndef _IFACE_H
#define _IFACE_H

#ifdef __cplusplus 
extern "C" {
#endif 

// Specification of storage-class information. It enables to export and import functions, data, and objects to and from a DLL..
#if defined(_WIN32) || defined(__WIN32__)
    #if defined(iface_EXPORTS)
        #define  IFACE_API __declspec(dllexport)
    #else
        #define  IFACE_API __declspec(dllimport)
    #endif
#elif defined(linux) || defined(__linux)
    #if __GNUC__ >= 4
        #define IFACE_API __attribute__ ((visibility ("default")))
    #else
        #define IFACE_API
    #endif
#endif

#include "iface_error_codes.h"
#include "iface_defs.h"
#include "iface_param_names.h"

/*
Summary:
    Returns the library version.
Parameters:
    major - [out] Major version of the library
    minor - [out] Minor version of the library
    revision - [out] Revision
Return value: 
    error code
*/
IFACE_API int IFACE_GetVersionInfo( int* major, int* minor, int* revision );

/*
Summary:
    Returns error message describing given error code.
Parameters:
    errorCode - [in] error code returned from IFace functions
    bufferSize - [in] size of buffer where the error message is written. 
                 If the message length is longer then the buffer size then the message is cut off.
    buffer - [out] buffer where the error message is written
Return value: 
    error code
*/
IFACE_API int IFACE_GetErrorMessage(int errorCode, unsigned int bufferSize, char* buffer);

/*
Summary:
    Returns hardware ID of device for licensing purposes.
Parameters:
    hwId - [out] Pointer to the memory where hardware ID will be saved. If hwId is set to NULL, only length is returned.
    length - [in/out] in - total size of allocated memory pointed by the hwId parameter; out - required or actually used size of hwId
Remarks:
    If <c>hwId==NULL<c/> length of hwId is calculated and returned, otherwise data are filled
Return value:
    error code
*/
IFACE_API int IFACE_GetHardwareId(char* hwId, int* length);

/*
Summary:
    Initializes IFace SDK and checks validity of IFace SDK software license from license file. 
Return value:
    error code
*/
IFACE_API int IFACE_Init();

/*
Summary:
    Terminates IFace and releases all used resources.
Return value:
    error code
*/
IFACE_API int IFACE_Terminate();

/*
Summary:
    Initializes IFACE SDK and checks validity of IFace SDK software license from given license data string.
Parameters:
    licenseData	- [in] license data
    licenseDataSize	- [in] license data size
Return value:
    error code
*/
IFACE_API int IFACE_InitWithLicence( const unsigned char *licenseData, const int licenseDataSize );

/*
Summary:
    Initializes and turns on IFace logger. Log messages related to IFace processing having different 
    severity levels are written into defined file or to stdout. 
Parameters:
    severityLevel - [in] logging severity level according to the IFACE_LoggerSeverityLevel enum
    dstPath - [in] null-terminated string containing path to the log file. If dstPath has value 
              NULL then log messages are redirected to stdout
Return value:
    error code
*/
IFACE_API int IFACE_SetLogger( IFACE_LoggerSeverityLevel severityLevel, char* dstPath );

/*
Summary:
    Creates and initializes face handler entity. Face handler entity contains data and parameters 
    related to face processing (face detection, face matching, etc.). 
Parameters:
    faceHandler - [out] pointer to newly created face handler entity if created successfully, NULL otherwise.
Return value: 
    error code
*/
IFACE_API int IFACE_CreateFaceHandler( void** faceHandler );

/*
Summary:
Creates empty face entity. After face detection and processing with face related API 
functions contains face entity specific data (features positions, face attributes, face template, etc.)
Parameters:
    face - [out] pointer to newly created face if created successfully, NULL otherwise. 
Return value:
    error code
*/
IFACE_API int IFACE_CreateFace( void** face );


/*
Summary:
    Serializes face entity content to byte array.
Parameters:
    face - [in] pointer to face entity
    faceHandler - [in] pointer to face handler entity
    serializedFace - [in/out] pointer to an allocated array that will be filled with serialized 
                     face entity data. If serializedFace==NULL then just size of serialized face entity 
                     is returned, otherwise data are filled.
    serializedFaceSize - [in/out] size of serialized face entity data

Return value:
    error code
*/
IFACE_API int IFACE_SerializeFace( void* face, void* faceHandler, char* serializedFace, int* serializedFaceSize);


/*
Summary:
    Deserialize face entity content from byte array.
Parameters:
    face - [in/out] pointer to face entity that will be filled with serialized face entity data
    faceHandler - [in] pointer to face handler entity
    serializedFace - [in] pointer to memory filled with serialized face entity data 
    serializedFaceSize - [in] length of data in serializedFace array
Return value:
    error code
*/
IFACE_API int IFACE_DeserializeFace( void* face, void* faceHandler, char* serializedFace, int serializedFaceSize);

/*
Summary:
    Sets value to defined user parameter. Parameter can be related to specific entity or can be global.  
Parameters:
    entity - [in] pointer to entity if parameter is entity related. If parameter is global then 
             the entity parameter must be set to IFACE_GLOBAL_PARAMETERS (NULL). 
             Global parameters can be set only before calling IFACE_Init() or after calling IFACE_Terminate(). 
             Global parameters are switched to read-only mode after calling IFACE_Init() and before calling IFACE_Terminate().
    parameterName - [in] parameter name
    parameterValue - [in] parameter value
Return value:
    error code
*/
IFACE_API int IFACE_SetParam( void* entity, const char* parameterName, const char* parameterValue );

/* 
Summary
   Gets user parameter size.
Parameters
    entity - [in] pointer to entity if parameter is entity related. If parameter is global then 
             the entity parameter must be set to IFACE_GLOBAL_PARAMETERS (NULL). 
    parameterName - [in] parameter name
    parameterValueSize - [out] size of buffer that must be allocated for the parameter value
Return value:
   error code                                                     
*/
IFACE_API int IFACE_GetParamSize( void* entity, const char* parameterName, unsigned int* parameterValueSize);

/*
Summary:
    Gets user parameter value.
Parameters:
    entity - [in] pointer to entity if parameter is entity related. If parameter is global then 
             the entity parameter must be set to IFACE_GLOBAL_PARAMETERS (NULL). 
    parameterName - [in] parameter name
    parameterValue - [out] parameter value
    parameterValueSize - [in] total size of allocated memory pointed by the value parameter.
Return value:
    error code
*/
IFACE_API int IFACE_GetParam( void* entity, const char* parameterName, char* parameterValue, unsigned int parameterValueSize);


/* 
Summary
   Detects all/requested count faces in given image and sets face detection results into given 
   preallocated face entities (using given face handler entity). 
Parameters
   rawImage - [in] pointer to raw image data, each pixel has 3 components (bytes) in BGR order
   width - [in] width of input image. Minimal valid width can be retrieved 
           from parameter IFACE_PARAMETER_GLOBAL_MIN_VALID_IMAGE_SIZE.  
   height - [in] height of input image. Minimal valid height can be retrieved 
            from parameter IFACE_PARAMETER_GLOBAL_MIN_VALID_IMAGE_SIZE. 
   minEyeDistance - [in] defines minimal size of detected faces. Size of face is defined by eyes distance in pixels.
                    Minimal valid value can be retrieved from parameter IFACE_PARAMETER_GLOBAL_MIN_VALID_EYES_DISTANCE.
   maxEyeDistance - [in] defines maximal size of detected faces. Size of face is defined by eyes distance in pixels.
   faceHandler - [in/out] pointer to face handler entity
   facesCount - [in/out] in: max count of faces to be detected in given image, out: count of detected faces
   faces - [in/out] array of pointers to face entities that should be filled with detected faces 
Return value:
   error code
*/
IFACE_API int IFACE_DetectFaces( 
    unsigned char* rawImage, int width, int height, int minEyeDistance, int maxEyeDistance, 
    void* faceHandler, int* facesCount, void** faces
);

/*
Summary
   Detects fixed number of faces at predefined positions in a given image and fills preallocated face entities using face handler entity.
Parameters
   rawImage - [in] pointer to raw image data, each pixel has 3 components (bytes) in BGR order
   width - [in] width of input image. Minimal valid width can be retrieved 
           from parameter IFACE_PARAMETER_GLOBAL_MIN_VALID_IMAGE_SIZE.  
   height - [in] height of input image. Minimal valid height can be retrieved 
            from parameter IFACE_PARAMETER_GLOBAL_MIN_VALID_IMAGE_SIZE. 
   facesCount - [in] count of faces, size of the following arrays
   rightEyesX - [in] x coordinates of right eyes
   rightEyesY - [in] y coordinates of right eyes
   leftEyesX - [in] x coordinates of left eyes
   leftEyesY - [in] y coordinates of left eyes
   faceHandler - [in/out] pointer to face handler entity
   faces - [in/out] array of pointers to face entities that should be filled with detected faces.
           Detected faces are confidence sorted using descending order, so the face with highest confidence 
           has the highest score.
Return value:
   error code
*/
IFACE_API int IFACE_DetectFacesAtPositions(unsigned char* rawImage, int width, int height, int facesCount,
    float* rightEyesX, float* rightEyesY, float* leftEyesX, float* leftEyesY,
    void* faceHandler, void** faces);

/*
Summary:
    Retrieves basic info about face (eyes position and face score).
Parameters:
    face - [in] pointer to face entity
    faceHandler - [in] pointer to face handler entity
    rightEyeX -   [out] x coordinate of right eye
    rightEyeY -   [out] y coordinate of right eye
    leftEyeX -    [out] x coordinate of left eye
    leftEyeY -    [out] y coordinate of left eye
    score -       [out] face score from range <0,10000>.
                        The higher the value of the score the better quality of the face.
Return value:
    error code
*/
IFACE_API int IFACE_GetFaceBasicInfo( void* face, void* faceHandler, float* rightEyeX, float* rightEyeY, float* leftEyeX, float* leftEyeY, float* score);

/*
Summary:
    Retrieves given facial features (position and score) using specified face. Position is in float precision.
Parameters:
    face - [in/out] pointer to face entity
    faceHandler - [in] pointer to face handler entity
    requestedFeatures - [in] array of requested features
    numFeatures - [in] number of requested features; requestedFeatures / posX / posY / score has this count of elements
    posX - [out] array of x coordinates related to requestedFeatures
    posY - [out] array of y coordinates related to requestedFeatures
    score - [out] array of scores related to requestedFeatures. 
            The score can have special values if feature is not detectable (IFACE_FACE_FEATURE_STATE_UNDETECTABLE) or 
            the score for the feature is not defined (IFACE_FACE_FEATURE_STATE_DETECTABLE_NO_CONFIDENCE). 
Return value: 
    error code
*/
IFACE_API int IFACE_GetFaceFeatures( void* face, void* faceHandler, IFACE_FaceFeatureId* requestedFeatures, int numFeatures, float* posX, float* posY, float* score );

// Functions IFACE_GetFaceAttribute, IFACE_GetFaceAttributeDependenciesStatus and IFACE_GetFaceAttributeRangeStatus 
// retrieve ICAO status of various ICAO features (some of face attributes, see IFACE_FaceAttributeId). Reliability of each 
// face attribute can be dependent on several other face attributes. For example eye-gaze status can be be correctly 
// evaluated only when face has frontal position and both eyes are open. It means that eye-gaze status is dependent on 
// features yaw, pitch, right eye-status and left eye status. Once the conditions defined on these referenced features
// are fulfilled then can be dependent ICAO feature correctly evaluated.
// The status consist of three parts: 
//
//  1. score of ICAO feature - score (value) of desired face attribute, which can be retrieved by 
//  function IFACE_GetFaceAttribute 
//
//  2. dependencies status of ICAO feature - fulfillment of conditions defined on referenced face attributes (desired 
//  feature is depending on them) can be retrieved by function IFACE_GetFaceAttributeDependenciesStatus. 
//  Possible values are enumerated in IFACE_FaceAttributeDependenciesStatus. 
//
//  3. reliability range status of ICAO feature - reliability range status of desired ICAO feature can be 
//  retrieved be function IFACE_GetFaceAttributeRangeStatus. Possible values are enumerated in 
//  IFACE_FaceAttributeRangeStatus.
//
// Once an ICAO feature score is in reliability range and the conditions defined on referenced face attributes 
// are fulfilled then the feature is ICAO compliant.

/*
Summary:
    Returns value of desired facial attribute. Attributes producing numeric score values 
    (e.g. mouth status, eyes status etc.) can be calculated  using this API function. 
    Moreover some other attributes like age, eye-distance, gender etc. can be 
    evaluated using this API function. Complete set of available attributes are enumerated 
    in IFACE_FaceAttributeId.  Attributes not returning numeric values can't be 
    passed into IFACE_GetFaceAttribute (e.g. segmentation mask).
    This API function can be used for ICAO compliance evaluation of some of face attributes.
Parameters:
    face - [in/out] pointer to face entity
    faceHandler - [in] pointer to face handler entity
    fattrId - [in] defines which facial attribute is evaluated.
           Possible values are enumerated in IFACE_FaceAttributeId.
    fattrValue - [out] value of desired facial attribute
Return value:
    error code
*/
IFACE_API int IFACE_GetFaceAttribute( void *face, void *faceHandler, const IFACE_FaceAttributeId fattrId, float *fattrValue );

/*
Summary:
    Returns raw (unnormalized) value of desired facial attribute. Complete set of available
    attributes are enumerated in IFACE_FaceAttributeId (only numeric attributes can be 
    passed into IFACE_GetFaceAttributeRaw)
Parameters:
    face - [in/out] pointer to face entity
    faceHandler - [in] pointer to face handler entity
    fattrId - [in] defines which facial attribute is evaluated.
              Possible values are enumerated in IFACE_FaceAttributeId.
    fattrValueRaw - [out] raw (unnormalized) value of desired facial attribute
Return value:
    error code
*/
IFACE_API int IFACE_GetFaceAttributeRaw( void *face, void *faceHandler, const IFACE_FaceAttributeId fattrId, float *fattrValueRaw );

/*
Summary:
    Retrieves fulfillment of dependencies which are necessary for valid 
    face attribute evaluation (desired attribute is depending on them). 
    If dependencies of desired feature are not fulfilled then validity 
    of face attribute is not guaranteed.
    This API function can be used for ICAO compliance evaluation of some of face attributes.
Parameters:
    face - [in/out] pointer to face entity
    faceHandler - [in/out] pointer to face handler entity
    fattrId - [in] defines which face attribute is evaluated. 
              Possible values are enumerated in IFACE_FaceAttributeId.
    fattrDependenciesStatus - [out] fulfillment of face attribute dependencies (desired attribute 
                              is depending on them). Possible values are enumerated in 
                              IFACE_FaceAttributeDependenciesStatus. 
Return value:
    error code
*/
IFACE_API int IFACE_GetFaceAttributeDependenciesStatus( void *face, void *faceHandler, const IFACE_FaceAttributeId fattrId, IFACE_FaceAttributeDependenciesStatus *fattrDependenciesStatus );

/*
Summary:
    Retrieves reliability range status of desired face attribute.
    Only features producing numeric values can be evaluated - see documentation of IFACE_FaceAttributeId.
    This API function can be used for ICAO compliance evaluation of some of face attributes.
Parameters:
    face - [in/out] pointer to face entity
    faceHandler - [in/out] pointer to face handler entity
    fattrId - [in] defines which face attribute is evaluated.
              Possible values are enumerated in IFACE_FaceAttributeId.
    rangeStatus - [out] range compliance status of desired face attribute. Possible values are enumerated in 
                  IFACE_FaceAttributeRangeStatus.
Return value:
    error code
*/
IFACE_API int IFACE_GetFaceAttributeRangeStatus( void *face, void *faceHandler, const IFACE_FaceAttributeId fattrId, IFACE_FaceAttributeRangeStatus *rangeStatus );

/*
Summary:
    Retrieves face image with background / foreground segmented. The segmentation image is cropped according to the selected 
    cropping method <c>IFACE_FaceCropMethod</c>. The types of segmentation can be selected from <c>IFACE_SegmentationImageType</c>.
    The segmentation of hairstyle can be tuned by setting matting to <c>IFACE_PARAMETER_SEGMENTATION_MATTING_TYPE</c> parameter.
Parameters:
    face - [in/out] pointer to face entity
    faceHandler - [in] pointer to face handler entity
    cropMethod - [in] face cropping method according to the IFACE_FaceCropMethod enum
    segImageType - [in] type of output segmentation image type according to the IFACE_SegmentationImageType enum
    segImageWidth - [out] width of segmentation image 
    segImageHeight - [out] height of segmentation image 
    segImageChannels - [out] count of segmentation image channels
    segImageLength - [out] segmentation image length
    segImageRaw - [out] image data to be filled, if segImageRaw==NULL, only segmentation image length (and dimensions) is returned
 Returns
    error code  
 */
IFACE_API int IFACE_GetFaceSegmentation( void* face, void* faceHandler, IFACE_FaceCropMethod cropMethod, IFACE_SegmentationImageType segImageType, 
    int* segImageWidth, int* segImageHeight, int* segImageChannels, int* segImageLength, unsigned char* segImageRaw);

/*
Summary:
    Retrieves face cropping rectangle according to the selected cropping method.
Parameters:
    face - [in/out] pointer to face entity
    faceHandler -  [in] pointer to face handler entity
    cropMethod -  [in] face cropping method according to the IFACE_FaceCropMethod enum
    cropRect -    [out] array of X and Y values of crop quadrilateral corners in following order:
                  X top-left, Y top-left, X top-right, Y top-right, X bottom-left, Y bottom-left, X bottom-right, Y bottom-right
Return value:
    error code
*/
IFACE_API int IFACE_GetFaceCropRectangle( void* face, void* faceHandler, IFACE_FaceCropMethod cropMethod, float cropRect[8] );

/*
Summary:
    Retrieves face image cropped according to the selected cropping method.
Parameters:
    face - [in/out] pointer to face entity
    faceHandler - [in] pointer to face handler entity
    cropMethod - [in] face cropping method according to the IFACE_FaceCropMethod enum
    cropWidth - [out] width of cropped image
    cropHeight - [out] height of cropped image
    cropLength - [out] cropped image length
    rawCroppedImage - [out] image data to be filled, if rawCroppedImage==NULL, only cropped image length is returned
Returns
    error code  
*/
IFACE_API int IFACE_GetFaceCropImage( void* face, void* faceHandler, IFACE_FaceCropMethod cropMethod, int* cropWidth, int* cropHeight, int* cropLength, unsigned char* rawCroppedImage);

/*
Summary:
    Retrieves face verification template.
Parameters:
    face - [in] pointer to face entity
    faceHandler - [in] pointer to face handler entity
    reserved - [in] reserved for future use
    templateSize - [in/out] template data length
    faceTemplate - [out] template data that can be used in function IFACE_MatchTemplate or IFACE_SearchTemplate
Remarks:
    If <c>faceTemplate==NULL<c/> template size is calculated and returned, otherwise data are filled
Return value:
    error code
*/
IFACE_API int IFACE_CreateTemplate( void* face, void* faceHandler, int reserved, int *templateSize, char *faceTemplate );

/*
Summary:
    Retrieves information about verification template
Parameters:
    faceHandler - [in] pointer to face handler entity
    faceTemplate - [in] face verification template
    majorVersion - [out] major version of the template
    minorVersion - [out] minor version of the template
Return value:
    error code
*/
IFACE_API int IFACE_GetTemplateInfo( void* faceHandler, char *faceTemplate, int *majorVersion, int *minorVersion );


/*
Summary:
    Retrieves array of face templates from array of face entities
Parameters:
    faceHandler - [in] pointer to face handler entity
    facesCount - [in] count of face in given face entities array
    faces - [in] array of face entities 
    templateSize - [in/out] template data length
    faceTemplates - [out] array of face templates
Return value:
    error code
*/
IFACE_API int IFACE_CreateTemplateBatch( void *faceHandler, int facesCount, void **faces, int *templateSize, char **faceTemplates );

/* 
Summary:
   Compares the similarity of two face templates and calculates
   their matching score. It is implementation of one to one
   matching known as verification.
Parameters:
   faceHandler - [in] pointer to face handler entity
   faceTemplate1 - [in] face verification template created by IFACE_CreateTemplate
   faceTemplate2 - [in] face verification template created by IFACE_CreateTemplate
   score - [out] matching score calculated as -10*log(far), score 30 is roughly at far 1:1000, score 50 is roughly at far 1:100000
Return value:
   error code
*/
IFACE_API int IFACE_MatchTemplate( void* faceHandler, char* faceTemplate1, char* faceTemplate2, float *score );

/* 
Summary:
   Searches the most similar face templates to a given probe
   template in set of templates. It is implementation of one to
   many matching known as identification.
Remarks:
   This function is not implemented yet. Just API is prepared.
Parameters:
   faceHandler - [in] pointer to face handler entity
   probeTemplate - [in] search for templates similar to this template
   searchTemplatesArray - [in] array of templates to search in
   searchTemplatesCount - [in] count of templates in searchTemplatesArray
   bestCandidatesCount - [in] count of the most similar templates to return
   bestCandidates - [out] indexes (related to searchTemplatesArray) of the most
                    similar templates in descending order, needs to be allocated to at least
                    'bestCandidatesCount * sizeof(int)'
   bestCandidatesScores - [out] scores of the most similar templates (related to bestCandidates),
                          needs to be allocated to at least 'bestCandidatesCount * sizeof(float)',
                          for more details see IFACE_MatchTemplate
Return value:
   error code
*/
IFACE_API int IFACE_SearchTemplate( void* faceHandler, char* probeTemplate, char** searchTemplatesArray, int searchTemplatesCount, int bestCandidatesCount, int* bestCandidates, float* bestCandidatesScores);

/*
Summary:
     Auxiliary function for loading source image from a memory buffer, into raw image format (appropriate for usage in IFace)
Parameters:
    imgData - [in] pointer to a memory buffer containing source image data (png, bmp, tif, jpg formats are supported)
    size - [in] size of the image
    width - [out] image width
    height - [out] image height
    length - [out] image data length
    rawImage - [out] prepared memory buffer to be filled with image data (each pixel has 3 components (bytes) in BGR order for raw image data).
               If rawImage==NULL then just source image dimensions and data length are returned. Otherwise data are filled.
Return value:
    error code
*/
IFACE_API int IFACE_LoadImageFromMemory( const char* imgData, int size,  int* width, int* height, int* length, unsigned char* rawImage );

/* 
Summary:
   Auxiliary function for loading source image from file, into
   raw image format (appropriate for usage in IFace)
Parameters:
   fileName - [in] file path of source image to be loaded (png,
              bmp, tif, jpg formats are supported)
   width - [out] image width
   height - [out] image height
   length - [out] image data length
   rawImage - [in/out] prepared memory buffer to be filled with
              raw image data (each pixel has 3 components
              (bytes) in BGR order). If rawImage==NULL then just
              source image dimensions and data length are
              returned. Otherwise data are filled.
Return value:
   error code                                                    
*/
IFACE_API int IFACE_LoadImage( const char* fileName, int* width, int* height, int* length, unsigned char* rawImage );

/*
Summary:
    Auxiliary function that encodes a raw image from memory buffer, into memory buffered image formated as rawChannels channels image of specified type (e.g. bmp, jpg, png format) in imageType
Parameters:
    rawImage - [in] memory buffer filled with raw image data (each pixel has 3 components (bytes) in BGR order)
    rawWidth - [in] width of the raw image
    rawHeight - [in] height of the raw image
    rawChannels - [in] number of channels of the raw image
    imageType - [in] type of image to be written into memory buffer according to IFACE_ImageSaveType
    imageSize - [in/out] size of the resulting image memory buffer
    image - [in/out] pointer to memory buffer (array of bytes to be filled resulting image).
            If image==NULL then just size of resulting image imageSize is returned, otherwise data are filled.

Return value:
    error code
*/
IFACE_API int IFACE_SaveImageToMemory( unsigned char* rawImage, int rawWidth, int rawHeight, int rawChannels, IFACE_ImageSaveType imageType, int * imageSize, unsigned char* image);

/* 
Summary:
   Auxiliary function for saving raw image data into file.
Parameters:
   fileName - [in] image file to be saved. Target format is
               defined by filename extension (png, bmp, tif, jpg formats are supported)
   width - [in] image width
   height - [in] image height
   channels - [in] image channels count
   rawImage - [in] raw image data (each pixel has 3 components
              (bytes) in BGR order or 4 components (bytes) in BGRA order (A stands for Alpha transparent channel))
Return value:
   error code                                                    
*/
IFACE_API int IFACE_SaveImage( const char* fileName, int width, int height, int channels, unsigned char* rawImage );

/*
Summary:
   Sets the face features into face entity. Only selected facial features are set.
Parameters:
   face - [in/out] pointer to face entity
   faceHandler - [in/out] pointer to face handler entity
   rawImage - [in] pointer to image data, 3 components in BGR order
   width - [in] image width
   height - [in] image height
   features - [in] array of selected facial features
   numFeatures - [in] number of selected facial features
   posX - [in] given x positions of selected facial features
   posY - [in] given y positions of selected facial features
Remarks:
   Auxiliary function which can be used for accuracy tests.
Return value:
   error code
*/
IFACE_API int IFACE_SetFaceFeatures( void* face, void *faceHandler,
    unsigned char* rawImage, int width, int height,
    IFACE_FaceFeatureId* features, int numFeatures, float *posX, float *posY );

/*
Summary:
    Creates and initializes object handler entity.
Parameters:
    objectHandler - [out] pointer to newly created object handler entity if created successfully, NULL otherwise.
    detectionHandler - [in] handler entity usable for object detection (currently face handler entity can be used only)
Return value: 
    error code
*/
IFACE_API int IFACE_CreateObjectHandler( void** objectHandler, void* detectionHandler );

/*
Summary:
    Creates empty object entity.
Parameters:
    object - [out] pointer to newly created object entity if created successfully, NULL otherwise.
Return value: 
    error code
*/
IFACE_API int IFACE_CreateObject( void** object );

/*
Summary:
    Performs tracking of objects in video sequence. Just one video frame (<c>rawImage</c>, 
    supplemented by time info <c>timeStampMs</c>) is processed in one call of 
    IFACE_TrackObjects function. The state of tracked objects is stored 
    in <c>objects</c> array. Objects of detection handler entity type (e.g. face handler entity for face objects) 
    set to object handler entity (in IFACE_CreateObjectHandler call) are detected and tracked.
Parameters:
    objectHandler - [in] pointer to object handler entity
    rawImage - [in] pointer to raw image data, each pixel has 3 components (bytes) in BGR order
    width - [in] width of input image. Minimal valid width can be retrieved 
            from parameter IFACE_PARAMETER_GLOBAL_MIN_VALID_IMAGE_SIZE.  
    height - [in] height of input image. Minimal valid height can be retrieved 
             from parameter IFACE_PARAMETER_GLOBAL_MIN_VALID_IMAGE_SIZE. 
    timeStampMs - [in] number of ms from the video first frame
    objectsCount - [in] count of tracked objects
    objects - [in/out] tracked object entities
Return value: 
    error code
*/
IFACE_API int IFACE_TrackObjects( void* objectHandler, unsigned char* rawImage, int width, int height, long long timeStampMs, int objectsCount, void** objects);

/*
Summary:
    Sets objects for tracking. Objects are defined by position 
    (<c>xs</c>, <c>ys</c>) and size (<c>widths</c>, <c>heights</c>).
Parameters:
    objectHandler - [in] pointer to object handler entity
    objectsCount - [in] count of object entities 
    objects - [in/out] array of pointers to object entities
    setObjectsCount - [in] number of objects that should be set. It defines the size of xs, ys, widths and heights array
    xs - [in] x-coordinates of objects bounding box
    ys - [in] y-coordinates of objects bounding box
    widths - [in] widths of objects bounding box
    heights - [in] heights of objects bounding box
Return value: 
    error code
*/
IFACE_API int IFACE_SetTrackingObjects( void* objectHandler, int objectsCount, void** objects, int setObjectsCount, int* xs, int* ys, int* widths, int* heights);

/*
Summary:
    Sets important areas of scene for tracking using given mask. The value 0 indicates 
    unimportant image parts that are not searched during object tracking.
Parameters:
    objectHandler - [in] pointer to objectHandler entity
    rawImage - [in] pointer to raw image data, each pixel has just one gray-scale component
    width - [in] width of input image. Minimal valid width can be retrieved 
            from parameter IFACE_PARAMETER_GLOBAL_MIN_VALID_IMAGE_SIZE.  
    height - [in] height of input image. Minimal valid height can be retrieved 
             from parameter IFACE_PARAMETER_GLOBAL_MIN_VALID_IMAGE_SIZE. 
Return value: 
    error code
*/
IFACE_API int IFACE_SetTrackingAreaMask( void* objectHandler, unsigned char* rawImage, int width, int height );

/*
Summary:
    Cleans object entity. The function deletes all internal object data (e.g. tracking information).
Parameters:
    object - [in] pointer to object entity
    objectHandler - [in] pointer to objectHandler entity
Return value: 
    error code
*/
IFACE_API int IFACE_CleanObject( void* object, void* objectHandler );

/*
Summary:
    Returns object id from given object entity.
Parameters:
    object - [in] pointer to object entity
    objectHandler - [in] pointer to objectHandler entity
    id - [out] id of the object
Return value: 
    error code
*/
IFACE_API int IFACE_GetObjectId( void* object, void* objectHandler, int* id);

/*
Summary:
    Returns object bounding box from given object entity.
Parameters:
    object - [in] pointer to object entity
    objectHandler - [in] pointer to objectHandler entity
    x - [out] x-coordinate of bounding box of the object
    y - [out] y-coordinate of bounding box of the object
    w - [out] width of bounding box of the object
    h - [out] height of bounding box of the object
Return value: 
    error code
*/
IFACE_API int IFACE_GetObjectBoundingBox( void* object, void* objectHandler, int* x, int* y, int* w, int* h);

/*
Summary:
    Returns object bounding box trajectory from given object entity.
Parameters:
    object - [in] pointer to object entity
    objectHandler - [in] pointer to objectHandler entity
    trajectoryLength - [in] number of bounding boxes from the tracking history
    x - [out] x-coordinates of bounding boxes of the object
    y - [out] y-coordinates of bounding boxes of the object
    w - [out] width of bounding boxes of the object
    h - [out] height of bounding boxes of the object
Return value: 
    error code
*/
IFACE_API int IFACE_GetObjectTrajectory( void* object, void* objectHandler, int trajectoryLength, int* x, int* y, int* w, int* h);

/*
Summary:
    Returns object state from given object entity.
Parameters:
    object - [in] pointer to object entity
    objectHandler - [in] pointer to objectHandler entity
    state - [out] State of the object (see details in <c>IFACE_TrackedObjectState</c>)
Return value: 
    error code
*/
IFACE_API int IFACE_GetObjectState( void* object, void* objectHandler, IFACE_TrackedObjectState* state);

/*
Summary:
    Returns object type from given object entity.
Parameters:
    object - [in] pointer to object entity
    objectHandler - [in] pointer to objectHandler entity
    type - [out] type of the object (see details in <c>IFace_TrackedObjectType</c>)
Return value: 
    error code
*/
IFACE_API int IFACE_GetObjectType( void* object, void* objectHandler, IFace_TrackedObjectType* type);

/*
Summary:
    Returns object score from given object entity.
Parameters:
    object - [in] pointer to object entity
    objectHandler - [in] pointer to objectHandler entity
    score - [out] score of the object from range <0,10000>.
            The higher the value of the score the higher confidence in the object.
Return value: 
    error code
*/
IFACE_API int IFACE_GetObjectScore( void* object, void* objectHandler, float* score);

/*
Summary:
    Function returns particular type of face entity related to given object entity.
Parameters:
    object - [in] pointer to object entity.
    objectHandler - [in] pointer to objectHandler entity
    face - [out] face entity stored within object entity copied to given preallocated face entity
    typeOfFace - [in] type of wanted face entity (see <c>IFACE_TrackedObjectFaceType</c>)
Return value: 
    error code
*/
IFACE_API int IFACE_GetFaceFromObject( void* object, void* objectHandler, void* face, IFACE_TrackedObjectFaceType typeOfFace);

/*
Summary:
    Function that clone entities. Currently only face entity is supported.
Parameters:
    entitySrc - [in] pointer to source entity.
    entityDst - [in/out] pointer to destination entity. Entity should be already allocated.
Return value: 
    error code
*/
IFACE_API int IFACE_CloneEntity( void* entitySrc, void* entityDst);


/*
Summary:
    Releases all types of data entities (face, faceHandler, objectHandler, object, etc).
Parameters:
    entity - [in] data entity to be released
Return value: 
    error code
*/
IFACE_API int IFACE_ReleaseEntity( void* entity);


#ifdef __cplusplus 
}
#endif 

#endif //_IFACE_H
