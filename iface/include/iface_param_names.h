// This header file contains list of all IFace SDK user paramters. Face SDK API Functions <c>IFACE_SetParam</c>, 
// <c>IFACE_GetParamSize</c> and <c>IFACE_GetParam</c> can be used for getting and setting of the user parameters.
#ifndef _IFACE_PARAMS_NAMES_H_
#define _IFACE_PARAMS_NAMES_H_

// IFace SDK has various internal parameters. Some of parameters can be adjusted to change behavior of IFace SDK.
// These parameters has its name and default values.  
//
// There are two main types of parameters:
//
//   * global parameters valid for whole IFace SDK
//
//   * entity parameters valid just within certain types of entities.
//
// IFace SDK API Functions IFACE_SetParam, IFACE_GetParamSize and IFACE_GetParam can be  
// used for getting and setting of the parameters. Names of parameters are strings as well as their values.

// Global parameters are valid for whole IFace, not just within entities.
// So, in case of global parameters, functions IFACE_SetParam, IFACE_GetParamSize and IFACE_GetParam 
// must be called with IFACE_GLOBAL_PARAMETERS standing for entity parameter. 
// Global parameters can be set only before calling IFACE_Init or after calling IFACE_Terminate.
// Global parameters are switched to read-only mode after IFACE_Init and before IFACE_Terminate.

#define IFACE_PARAMETER_GLOBAL_THREAD_NUM              "global.thrd.num"             // Parameter defining maximum number of parallel threads used within one entity.
                                                                                     // <p><i>Value type</i>: Integer, Read-Write

#define IFACE_PARAMETER_GLOBAL_THREAD_MANAGEMENT_MODE  "global.thrd.management_mode" // Parameter defining multi-threading mode (trade-off between speed and memory usage)
                                                                                     // <p><i>Value type</i>: String, Read-Write

#define IFACE_PARAMETER_GLOBAL_GPU_ENABLED             "global.gpu.enabled"          // Parameter defining if the CUDA GPU acceleration is enabled. 
                                                                                     // <p><i>Note: works only with special CUDA GPU optimized version of IFace</i>
                                                                                     // <p><i>Value type</i>: Boolean, Read-Write

#define IFACE_PARAMETER_GLOBAL_GPU_DEVICE_ID           "global.gpu.device_id"        // Parameter defining the device id which will be used for GPU CUDA computations
                                                                                     // <p><i>Note: works only if CUDA GPU optimization enabled and with special CUDA 
                                                                                     // GPU optimized version of IFace</i>
                                                                                     // <p><i>Value type</i>: Integer, Read-Write

#define IFACE_PARAMETER_GLOBAL_MIN_VALID_EYES_DISTANCE "global.fd.min_valid_eyes_distance"  // Parameter specifying minimal valid size of face which is detectable by IFace.
                                                                                         // \Face size is given by distance of face eyes in image. It is measured in pixels.
                                                                                         // <p><i>Value type</i>: Integer, Read-Only

#define IFACE_PARAMETER_GLOBAL_MIN_VALID_IMAGE_SIZE    "global.fd.min_valid_image_size"  // Parameter specifying minimal valid size (width or height) of image accepted by IFace.
                                                                                      // <p><i>Value type</i>: Integer, Read-Only

// Entity parameters are valid just within the entity. Each entity can have its own set of parameters if necessary.
// So, in case of entity parameters, functions IFACE_SetParam, IFACE_GetParamSize and IFACE_GetParam 
// must be called with valid entity pointer standing for entity parameter.

// Parameters specifying behavior of Face processing API functions. They are stored within face handler entity.
#define IFACE_PARAMETER_FACEDET_SPEED_ACCURACY_MODE    "fd.speed_accuracy_mode"     // Parameter defining face detection mode, which is trade-off between detection speed and 
                                                                                    // face and facial features detection accuracy. Change of this parameter change also 
                                                                                    // value of parameter <c>IFACE_PARAMETER_FACEDET_CONFIDENCE_THRESHOLD<c> because each
                                                                                    // mode has its own optimal threshold
                                                                                    // <p><i>Value type</i>: String, Read-Write

#define IFACE_PARAMETER_FACEDET_CONFIDENCE_THRESHOLD    "fd.face_confidence_threshold"  // Parameter defining minimal detections score. Internal detections with lower 
                                                                                       // score will not be returned from SDK

#define IFACE_PARAMETER_FACEVERIF_SPEED_ACCURACY_MODE  "fv.speed_accuracy_mode"     // Parameter defining face verification mode, which is trade-off between 
                                                                                    // face template creation speed and face template quality.
                                                                                    // <p><i>Value type</i>: String, Read-Write

#define IFACE_PARAMETER_BACKGROUND_COLOR               "img.background_color"       // Parameter defining color which is used to fill in parts of cropped image that 
                                                                                    // fall outside the original source image boundaries. 
                                                                                    // Valid value is hexadecimal code string e.g. "RRGGBB".
                                                                                    // <p><i>Value type</i>: String, Read-Write

#define IFACE_PARAMETER_GET_LIST_OF_ALL_PARAMS         "pa.get_list_of_all_params"  // Parameter containing all available parameters names.
                                                                                    // <p><i>Value type</i>: String, Read-Only

#define IFACE_PARAMETER_FACE_CROP_FULLFRONTAL_EXTENDED_SCALE  "img.face_crop_full_frontal_extended_scale" // Parameter defining amount of background image area enlargement of fully-frontal cropped facial image.
                                                                                                          // <p><i>Value type</i>: Int, Read-Write

#define IFACE_PARAMETER_SEGMENTATION_MATTING_TYPE          "hss.matting_type"       // Parameter defining type of matting used after head shoulder segmentation.
                                                                                    // <p><i>Value type</i>: String, Read-Write

// Names of parameters containing definitions of face attribute reliability conditions.
// These condition are evaluated in IFACE_GetFaceAttributeDependenciesStatus and IFACE_GetIcaoComplianceStatus and use following syntax:
//
// - For features with score: "FT:(FT_low;FT_high)[&&DEP1[:(DEP1_low;DEP1_high)][&&DEP2...[&&DEPn[:(DEPn_low;DEPn_high)]]]]"
//
// - For features without score: "[DEP1[:(DEP1_low;DEP1_high)][&&DEP2...[&&DEPn[:(DEPn_low;DEPn_high)]]]]"
//
// Where:	
//
//  - FT is name of desired feature (face attribute)
//
//  - (FT_low;FT_high) is reliability range for desired feature score  
//
//  - DEP1..DEPn are names of features that desired feature depends on
//
//  - (DEPx_low;DEPx_high) is reliability range of DEPx feature score 
//
// Notes: 
//
//  - Ranges can use any combination of two types of brackets - () and <>, 
//    where '()' defines an open interval and '<>' defines a closed interval.  
//
//  - DEPx must be feature producing score (numeric value). 
//
//  - It is not necessary for DEPx to have a range defined. If the range is not 
//    defined for DEPx in dependencies specification, and only name of feature DEPx
//    figures there, then range of desired feature having name of DEPX is used instead.
//
//  - Make sure to avoid loops in dependencies (FT1 depends on FT2 and FT2 depends on FT1).
//
//  - Please note that it is not recommended to change IFACE_PARAMETER_ICAO_COND_* parameters without serious reason.
//
// Examples:
//   See valid examples in IFACE_PARAMETER_FACE_ATTRIBUTE_COND_*_DEFAULT

#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_SHARPNESS                "faceattr.condition.sharpness"               // Parameter defining reliability condition and dependencies for facial sharpness
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_BRIGHTNESS               "faceattr.condition.brightness"              // Parameter defining reliability condition and dependencies for facial brightness
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_CONTRAST                 "faceattr.condition.contrast"                // Parameter defining reliability condition and dependencies for facial contrast
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_UNIQUE_INTENSITY_LEVELS  "faceattr.condition.unique_intensity_level"  // Parameter defining reliability condition and dependencies for facial unique intensity levels
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_SHADOW                   "faceattr.condition.shadow"                  // Parameter defining reliability condition and dependencies for facial shadow
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_NOSE_SHADOW              "faceattr.condition.nose_shadow"             // Parameter defining reliability condition and dependencies for facial sharp shadows
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_SPECULARITY              "faceattr.condition.specularity"             // Parameter defining reliability condition and dependencies for facial specularity
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_EYE_GAZE                 "faceattr.condition.eye_gaze"                // Parameter defining reliability condition and dependencies for eye gaze
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_EYE_STATUS_R             "faceattr.condition.eye_status_r"            // Parameter defining reliability condition and dependencies for right eye statue
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_EYE_STATUS_L             "faceattr.condition.eye_status_l"            // Parameter defining reliability condition and dependencies for left eye status  
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_GLASS_STATUS             "faceattr.condition.glass_status"            // Parameter defining reliability condition and dependencies for glass status 
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_HEAVY_FRAME              "faceattr.condition.heavy_frame"             // Parameter defining reliability condition and dependencies for heavy frame  
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_MOUTH_STATUS             "faceattr.condition.mouth_status"            // Parameter defining reliability condition and dependencies for mouth status   
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_BACKGROUND_UNIFORMITY    "faceattr.condition.background_uniformity"   // Parameter defining reliability condition and dependencies for background uniformity   
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_AGE                      "faceattr.condition.age"                     // Parameter defining reliability dependencies for person age estimation
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_RED_EYE_R                "faceattr.condition.red_eye_r"               // Parameter defining reliability condition and dependencies for red-eye effect on right eye
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_RED_EYE_L                "faceattr.condition.red_eye_l"               // Parameter defining reliability condition and dependencies for red-eye effect on left eye
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_ROLL                     "faceattr.condition.roll"                    // Parameter defining reliability condition and dependencies for head orientation - roll 
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_YAW                      "faceattr.condition.yaw"                     // Parameter defining reliability condition and dependencies for head orientation - yaw 
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_PITCH                    "faceattr.condition.pitch"                   // Parameter defining reliability condition and dependencies for head orientation - pitch
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_GENDER                   "faceattr.condition.gender"                  // Parameter defining reliability dependencies for gender estimation 
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_SEGMENTATION_MASK        "faceattr.condition.segmentation_mask"        // Parameter defining reliability dependencies for segmentation mask estimation
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_CROP                     "faceattr.condition.crop"                    // Parameter defining reliability dependencies for face cropping rectangle estimation
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_TEMPLATE                 "faceattr.condition.template"                // Parameter defining reliability dependencies for recognition template creation
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_EYE_DISTANCE             "faceattr.condition.eye_distance"            // Parameter defining reliability dependencies for eye distance estimation
#define IFACE_PARAMETER_FACE_ATTRIBUTE_COND_FACE_CONFIDENCE          "faceattr.condition.face_confidence"         // Parameter defining reliability dependencies for face detecion

// Parameters specifying behavior of object processing API functions. They are stored within object handler entity.

#define IFACE_PARAMETER_TRACK_MIN_EYE_DISTANCE             "track.fd.min_ed"                        // Parameter defining minimal eye distance of faces detected in discovery frames.
                                                                                                    // <p><i>Value type</i>: Integer, Read-Write
#define IFACE_PARAMETER_TRACK_MAX_EYE_DISTANCE             "track.fd.max_ed"                        // Parameter defining maximal eye distance of faces detected in discovery frames.
                                                                                                    // <p><i>Value type</i>: Integer, Read-Write
#define IFACE_PARAMETER_TRACK_FACE_DISCOVERY_FREQUENCE_MS  "track.fd.discovery_frequence_ms"        // Parameter defining how often often discovery (full frame face detection) frames appear (in milliseconds).
                                                                                                    // This parameter has influence on tracking performance - the more often discovery frames appears the more
                                                                                                    // the slower but more accurate is the tracking.
                                                                                                    // <p><i>Value type</i>: Integer, Read-Write
#define IFACE_PARAMETER_TRACK_DEEP_TRACK                   "track.deep_track"                       // Parameter defining whether face entity is obtainable from tracked object in every video frame ('true') 
                                                                                                    // or not ('false'). Please note that both eyes must be visible (trackable) if deep tracking 
                                                                                                    // should work. This parameter has influence on tracking performance - if set to 'false' then tracking is faster.
                                                                                                    // <p><i>Value type</i>: Boolean, Read-Write
#define IFACE_PARAMETER_TRACK_MOTION_OPTIMIZATION           "track.motion_optimization"             // Parameter defining how video motion detection in video influence object (face) detection in object tracking. 
                                                                                                    // Motion in video can define areas where objects move and object detection can be performed only within these areas.
                                                                                                    // This parameter has influence on tracking performance.
                                                                                                    // <p><i>Value type</i>: String, Read-Write
#endif